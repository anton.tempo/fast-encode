builtins.fetchGit {
  url = "https://github.com/NixOS/nixpkgs.git";
  ref = "master";
  rev = "5a0d0ec1cf90aae4354a97a2afaff079605ffc82";
}
