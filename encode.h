#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define ENCODE_STATUS_OK 0
#define ENCODE_STATUS_BAD_ALIGNMENT 1

// Preconditions:
//  * Both output and input arrays are continuous pixels (this means that stride
//    must be exactly width*sizeof(pixel) bytes)
//  * pixels is a multiple of 8
// Returns:
//  * ENCODE_STATUS_OK if all went well,
//  * ENCODE_STATUS_BAD_ALIGNMENT if either:
//    * pixels is not a multiple of 16
//    * in_float16_meters_depth is not aligned on 16 bytes
//    * out_BGRA is not aligned on 32 bytes
int encode_float16_meters_depth_to_BGRA(uint8_t *out_BGRA,
                                        __fp16 *in_float16_meters_depth,
                                        int pixels);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus
