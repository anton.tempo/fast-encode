let nixpkgs = import ./nixpkgs.nix;
    hostPkgs = import nixpkgs {};
    armPkgs = import nixpkgs {
      crossSystem = {
        config = "aarch64-unknown-linux-gnu";
      };
    };
in armPkgs.callPackage (
  {mkShell, cmake, clang-tools, gdb, gtest}:
  mkShell {
    nativeBuildInputs = [
      cmake (clang-tools.override { llvmPackages = hostPkgs.llvmPackages_12; })
      gdb hostPkgs.qemu
    ];
    buildInputs = [ gtest ];
  }
) { }
