#include "encode.h"

#include <arm_neon.h>

const float16_t LOWER_BOUND = 1.5;
const float16_t UPPER_BOUND = 2.8;
const float16_t SCALE = 256. / (UPPER_BOUND - LOWER_BOUND);

int encode_float16_meters_depth_to_BGRA(uint8_t *out_BGRA,
                                        __fp16 *in_float16_meters_depth,
                                        int pixels) {
  if ((pixels % 8) != 0) {
    return ENCODE_STATUS_BAD_ALIGNMENT;
  }

  // Amount of 8-float16 blocks in the input
  float16x8_t upper_bound = vmovq_n_f16(UPPER_BOUND);
  float16x8_t scale = vmovq_n_f16(SCALE);
  uint8x8_t fixed_255 = vmov_n_u8(255);
  float16x8_t upper_limit = vmovq_n_f16(255.);
  float16x8_t ffs = vmovq_n_f16(256.);

  int num_f16x8 = pixels / 8;
  for (int i = 0; i < num_f16x8; ++i) {
    float16x8_t depths = vld1q_f16(in_float16_meters_depth + i * 8);

    // depth = UPPER_BOUND - depth
    depths = vsubq_f16(upper_bound, depths);

    // depth = depth * SCALE. Values larger than 255 will get clamped automatically
    depths = vmulq_f16(depths, scale);

    // Turn values that are greater than 256 to 0
    uint16x8_t flags = vcltq_f16(depths, ffs);
    depths = (float16x8_t)(vandq_s8((int8x16_t)depths, (int8x16_t)flags));

    // Convert to 16-bit unsigned
    uint16x8_t in_uints = vcvtnq_u16_f16(depths);
    uint8x8_t in_uints8 = vqmovn_u16(in_uints);
    uint8x8x4_t bgra = { in_uints8, in_uints8, in_uints8, fixed_255 };
    vst4_u8(out_BGRA + i * 8 * 4, bgra);
  }

  return ENCODE_STATUS_OK;
}
