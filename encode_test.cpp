#include <cmath>
#include <cstdint>
#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <limits>

#include "encode.h"

TEST(FastEncodeTest, Zeros) {
  __fp16 in_floats[] = {
    .0f, .0f, .0f, .0f,
    .0f, .0f, .0f, .0f,
    .0f, .0f, .0f, .0f,
    .0f, .0f, .0f, .0f
  };
  uint8_t out_BGRA[] = {
    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,

    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,

    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,

    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,
  };

  int result = encode_float16_meters_depth_to_BGRA(out_BGRA, in_floats, 16);
  ASSERT_EQ(result, ENCODE_STATUS_OK);

  ASSERT_THAT(out_BGRA, testing::ElementsAre(
    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,

    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,

    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,

    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255
  ));
}

TEST(FastEncodeTest, NaN) {
  auto nan = std::numeric_limits<__fp16>::signaling_NaN();
  __fp16 in_floats[] = {
    nan, nan, nan, nan,
    nan, nan, nan, nan
  };

  uint8_t out_BGRA[] = {
    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,

    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,
    127, 127, 127, 127,
  };

  int result = encode_float16_meters_depth_to_BGRA(out_BGRA, in_floats, 16);
  ASSERT_EQ(result, ENCODE_STATUS_OK);

  ASSERT_THAT(out_BGRA, testing::ElementsAre(
    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,

    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255,
    0, 0, 0, 255
  ));
}

#pragma pack(push, 1)
struct BGRAPixel {
  uint8_t b;
  uint8_t g;
  uint8_t r;
  uint8_t a;
};
#pragma pack(pop)

void reference(uint8_t *out_BGRA, __fp16 *in_float16_meters_depth, int pixels) {
  const auto LOWER_BOUND = 1.5;
  const auto UPPER_BOUND = 2.8;
  const BGRAPixel invalidPixel = BGRAPixel { .b = 0, .g = 0, .r = 0, .a = 255};
  const __fp16 scale = 256. / (UPPER_BOUND - LOWER_BOUND);

  for (int i = 0; i < pixels; ++i) {
    __fp16 depth = in_float16_meters_depth[i];

    *(reinterpret_cast<BGRAPixel*>(out_BGRA) + i) = [&]() {
      if (depth < LOWER_BOUND || depth > UPPER_BOUND) {
        return invalidPixel;
      }

      __fp16 intensityValue = (UPPER_BOUND - depth) * scale;
      if (std::isnan(intensityValue) || std::isinf(intensityValue)) {
        return invalidPixel;
      }

      uint8_t intensity = (uint8_t)intensityValue;
      return BGRAPixel { .b = intensity, .g = intensity, .r = intensity, .a = 255 };
    }();
  }
}

TEST(FastEncodeTest, Reference) {
  __fp16 nan = std::numeric_limits<__fp16>::signaling_NaN();
  __fp16 pinf = std::numeric_limits<__fp16>::infinity();
  __fp16 ninf = -pinf;

  __fp16 in_floats[] = {
    nan, pinf, ninf, 0.0,
    1.2, 1.4, 1.5, 1.7,

    1.9, 2.2, 2.44, 2.5,
    2.8, 2.9, 3.1, 3.2
  };

  uint8_t out_BGRA[4 * std::size(in_floats)];
  uint8_t out_BGRA_ref[4 * std::size(in_floats)];

  reference(out_BGRA_ref, in_floats, std::size(in_floats));
  int result = encode_float16_meters_depth_to_BGRA(out_BGRA, in_floats, std::size(in_floats));
  ASSERT_EQ(result, ENCODE_STATUS_OK);

  ASSERT_THAT(out_BGRA, testing::ElementsAreArray(out_BGRA_ref));
}
